﻿using Enum.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enum.Repository
{
    internal interface IOrder
    {
        void Add(Product product);
        void Update();
        void Delete();
    }
}
