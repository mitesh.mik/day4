﻿using Enum.Constants;
using Enum.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enum.Repository
{
    internal class UserRepository : IOrder, IBillable
    {
        Product[] products = new Product[2];
        int count=0;
        public void Add(Product product)
        {
            products[count++] = product;
        }

        public void Delete()
        {
            Console.WriteLine("enter name want to delete");
            var name = Console.ReadLine();
            var del = Array.Find(products, item => item.Name == name);

            if (del == null)
            {
                Console.WriteLine("product doesnot exists"); 
            }
            else
            {
                var pro = Array.FindAll(products, item => item.Name != name);
                foreach (var p in pro)
                {
                    Console.WriteLine($"name:{p.Name} \t price:{p.Price} \t Quantity:{p.Quantity}");
                }
            }
        }

        public void GenerateInvoice(InvoiceOptions invoiceOptions)
        {
            double totalamount=0;
           foreach (Product item in products)
            {
                if (item == null) break;
               double amount = item.Price * item.Quantity;
                totalamount += amount;

            }
            Console.WriteLine($"totalamount:{totalamount}");
            switch (invoiceOptions)
            {
                case InvoiceOptions.Email:
                    Console.WriteLine("invoice send by mail");
                    break;
                case InvoiceOptions.Sms:
                    Console.WriteLine("invoice send by sms");
                    break;
                case InvoiceOptions.Pdf:
                    Console.WriteLine("invoice in the form of pdf download it.");
                    break;
                case InvoiceOptions.Print:
                    Console.WriteLine("Print the invoice");
                    break;
                default:
                    Console.WriteLine("wrong choice");
                    break;

            }
               
        }

        public Product[] getProducts()
        {
            return products;
        }

        public void Update()
        {
            throw new NotImplementedException();
        }
    }
}
