﻿using Enum.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enum.Repository
{
    internal interface IBillable
    {
        void GenerateInvoice(InvoiceOptions invoiceOptions);
    }
}
