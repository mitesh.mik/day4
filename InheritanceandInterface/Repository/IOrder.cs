﻿using InheritanceandInterface.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InheritanceandInterface.Repository
{
    internal interface IOrder
    {
        List<Product> BookOrder();
        void CancelOrder();
    }
}
