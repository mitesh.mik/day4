﻿using InheritanceandInterface.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InheritanceandInterface.Repository
{
    internal interface ICart
    {
        void AddtoCart(Product product);
        void RemoveProduct(string name);
    }
}
