﻿using InheritanceandInterface.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InheritanceandInterface.Repository
{
    internal class ProductRepository : IOrder, ICart
    {
        List<Product> products;
        public ProductRepository()
        {
            products = new List<Product>();
        }
        public void AddtoCart(Product product)
        {
            products.Add(product);
            Console.WriteLine("product added successfully");
        }

        public List<Product> BookOrder()
        {
            return products;

        }

        public void CancelOrder()
        {
            Console.WriteLine("your order is cancel.");
        }

     

        //void ICart.CancelOrder()
        //{
        //    Console.WriteLine("your order is cancel from ICart inteface.");
        //}
        public void RemoveProduct(string name)
        {
            string Name = name;
            products.RemoveAll(x => x.Name == Name);
            Console.WriteLine("product is remove");

        }

       
    }
}
