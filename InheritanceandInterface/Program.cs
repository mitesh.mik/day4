﻿
using InheritanceandInterface.Model;
using InheritanceandInterface.Repository;

ProductRepository prepo = new ProductRepository();
Product product = new Product() { Name="Lenovo",Price=30000};
prepo.AddtoCart(product);
Product product1 = new Product() { Name = "redmi", Price = 40000 };
prepo.AddtoCart(product1);
List<Product> pr=prepo.BookOrder();
foreach (Product p in pr)
{
    Console.WriteLine($"Name:{p.Name} \t Price:{p.Price}");
}

//ICart icart = (ICart)prepo;
//icart.CancelOrder();


Console.WriteLine("enter product want to  remove");
string name = Console.ReadLine();
prepo.RemoveProduct(name);