﻿// See https://aka.ms/new-console-template for more information
using System.Text.RegularExpressions;
//Has minimum 8 characters in length. {8,}
//At least one uppercase English letter.(?=.*?[A-Z])
//At least one lowercase English letter. (?=.*?[a-z])
//At least one digit. (?=.*?[0-9])
//At least one special character,(?=.*?[#?!@$%^&*-])

public static class Validate
{
    public const string pattern = @"^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$";
    public static bool validatePassword(string pass)
    {
        if (pass != null) return Regex.IsMatch(pass, pattern);
        else return false;

    }


}
public class MyClass
{
   public static void Main()
       
    {
        Console.WriteLine("enter password:");
        string password=Console.ReadLine();
        Console.WriteLine( Validate.validatePassword(password));
    }
}
